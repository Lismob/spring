package org.configuration;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Student && ((Student) bean).getName().equals("Jil")) {
            ((Student) bean).setMap(((Student) bean).getMap()
                    .entrySet().stream()
                    .collect(Collectors
                            .toMap(Map.Entry::getKey, (x) -> x.getValue() + 1)));
            System.out.println("Сработал postProcessBeforeInitialization для студента Jil");
        }
        return bean;
    }
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
