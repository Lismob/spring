package org.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component()
public class Teacher {
    private List<Student> listStud;

    @Autowired
    public Teacher(Student... student) {
        this.listStud = new ArrayList<>();
        listStud.addAll(Arrays.asList(student));
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "listStud=" + listStud +
                '}';
    }
}
