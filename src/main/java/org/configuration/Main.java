package org.configuration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        AbstractApplicationContext context
                = new AnnotationConfigApplicationContext(Config.class);
        System.out.println(context.getBean("studentSasha"));
        System.out.println(context.getBean("studentMasha"));
        System.out.println(context.getBean("studentJil"));
        System.out.println(context.getBean("teacher", Teacher.class) + "\n");
        context.close();


        ClassPathXmlApplicationContext contextXML
                = new ClassPathXmlApplicationContext(
                "configXML.xml");
        System.out.println(contextXML.getBean("studentSasha"));
        System.out.println(contextXML.getBean("studentMasha"));
        System.out.println(contextXML.getBean("studentJil"));
        System.out.println(contextXML.getBean("teacher"));
        contextXML.close();
    }
}

