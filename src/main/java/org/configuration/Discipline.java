package org.configuration;

public enum Discipline {
    MATHEMATICS,
    RUSSIAN_LANGUAGE,
    PHYSICS,
    LITERATURE
}
