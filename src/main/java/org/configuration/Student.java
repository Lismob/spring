package org.configuration;

import jakarta.annotation.PostConstruct;

import java.util.Map;

public class Student {
    private String name;
    private Map<Discipline, Integer> map;

    private boolean flag = false;

    public Student(String name, Map<Discipline, Integer> map) {
        this.name = name;
        this.map = map;
    }

    @PostConstruct()
    public void init() {
        if (map.values().stream().reduce(Integer::sum).map((x) -> x / map.size()).get() >= 3) {
            flag = true;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Student{" + "name='" + name + '\'' + ", map=" + map + ", flag=" + flag + '}';
    }

    public String getName() {
        return name;
    }

    public Map<Discipline, Integer> getMap() {
        return map;
    }

    public void setMap(Map<Discipline, Integer> map) {
        this.map = map;
    }
}
