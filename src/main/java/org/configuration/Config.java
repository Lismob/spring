package org.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("org.configuration")
class Config {
    @Bean
    public Student studentSasha() {
        Map<Discipline, Integer> map = new HashMap<>();
        map.put(Discipline.MATHEMATICS, 4);
        map.put(Discipline.RUSSIAN_LANGUAGE, 4);
        map.put(Discipline.LITERATURE, 3);
        map.put(Discipline.PHYSICS, 5);
        return new Student("Sasha", map);
    }

    @Bean
    public Student studentMasha() {
        Map<Discipline, Integer> map = new HashMap<>();
        map.put(Discipline.MATHEMATICS, 5);
        map.put(Discipline.RUSSIAN_LANGUAGE, 5);
        map.put(Discipline.LITERATURE, 5);
        map.put(Discipline.PHYSICS, 4);
        return new Student("Masha", map);
    }

    @Bean
    public Student studentJil() {
        Map<Discipline, Integer> map = new HashMap<>();
        map.put(Discipline.MATHEMATICS, 2);
        map.put(Discipline.RUSSIAN_LANGUAGE, 2);
        map.put(Discipline.LITERATURE, 3);
        map.put(Discipline.PHYSICS, 3);
        return new Student("Jil", map);
    }

    @Bean
    public MyBeanPostProcessor myBeanPostProcessor() {
        return new MyBeanPostProcessor();
    }
}
